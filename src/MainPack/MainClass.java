
package MainPack;

import GraphPack.MyGraphController;
import View.MainFrame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.graphstream.ui.swingViewer.ViewPanel;

/**
 *
 * @author bender
 */
public class MainClass
{


    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        MainFrame mainFrame = new MainFrame();                              // create our GUI
        MyGraphController mGraphModel = new MyGraphController(mainFrame);  // create our graph listener
        ViewPanel vw = mGraphModel.getViewPanel();  // get the view
        if (vw != null)
            mainFrame.setViewPanel(vw);
        mainFrame.setTitle("ProcessGraph");
        mainFrame.setVisible(true);

        displayContents(mainFrame, mGraphModel);
        cleanup(mainFrame, mGraphModel);
    }


    /**
     * Loop until exit signal
     */
    private static void displayContents(MainFrame frame, MyGraphController gc)
    {
        while (!frame.isClosed()) {
            try {
                gc.loop();
                frame.repaint();
                Thread.sleep(4000);
            } catch (InterruptedException ex) {
                Logger.getLogger("main class").log(Level.SEVERE, null, ex);
                break;
            }
        }
    }

    private static void cleanup(MainFrame frame, MyGraphController gc)
    {
        gc.saveGraphImage("Vangel.png", 1280, 1024);
        frame.close();
        gc.close(frame);
        System.exit(0);
    }
}
