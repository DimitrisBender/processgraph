package GraphPack;

import Processes.MyProcess;
import java.io.IOException;
import java.util.logging.Logger;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.DefaultGraph;
import org.graphstream.stream.file.FileSinkImages;


/**
 * Create a process graph with GraphStream
 * @author bender
 */
public class MyGraph
{    
    private static final Logger LOGGER = Logger.getGlobal();    // system log
    private final Graph graph;                                  // Directed graph
    private String CSSvalue;


    /**
     * Empty constructor. Creates a new DefaultGraph.
     */
    public MyGraph()
    {
        graph = new DefaultGraph("example", false, true);    // new graph with only one edge between two nodes and automatic creation of nodes
        graph.setStrict(false);
        //setCSSvalue("graph { fill-color: #808080; }");
        graph.addAttribute("ui.quality");
        graph.addAttribute("ui.antialias");
    }


    /**
     * get graph
     * @return DefaultGraph instance
     */
    protected Graph getGraph() {
        return graph;
    }


    /**
     * add a new child in our process graph
     * @param parent parent process
     * @param child child process
     */
    public void addChild(MyProcess parent, MyProcess child)
    {
        graph.addEdge(String.valueOf(child.getPid()), parent.getpName(), child.getpName(), true);

        Node p = graph.getNode(parent.getpName());
        p = graph.getNode(parent.getpName());           // get parent node
        p.addAttribute("ui.label", parent.getpName());  // node label is process command
        p.addAttribute("process", parent);              // bind parent process with node
        p.addAttribute("ui.style", parent.getCSS());    // prettify node
        p.addAttribute("ui.class", parent.getPermissions().name());

        Node n = graph.getNode(child.getpName());       // get children node
        n.addAttribute("ui.label", child.getpName());
        n.addAttribute("ui.class", child.getPermissions().name());
        n.addAttribute("process", child);               // bind process instance with the node
        n.addAttribute("ui.style", child.getCSS());
    }


    public void removeNode(String pid, String processName)
    {
        graph.removeEdge(pid);
        graph.removeNode(processName);
    }
    
    /**
     * Get graph CSS
     *
     * @return Valid CSS String
     */
    public String getCSSvalue() {
        return CSSvalue;
    }


    /**
     * Set graph CSS
     *
     * @param CSSvalue new valid CSS String
     */
    public void setCSSvalue(String CSSvalue) {
        this.CSSvalue = CSSvalue;
        graph.addAttribute("ui.stylesheet", getCSSvalue());
    }


    /**
     * Save Graph into a PNG image
     * @param filename name of the output file
     * @param width image width
     * @param height image height
     * @throws IOException in case of IOException
     */
    protected void saveGraphToPNG(String filename, int width, int height)
            throws IOException
    {
        if (graph == null)
            return;
        if ((width < 0 || width > 1920) || (height < 0 || height > 1080))   // bounds check
            return;
        if (filename == null || filename.isEmpty())
            return;

        FileSinkImages fsi = new FileSinkImages(
                FileSinkImages.OutputType.PNG,
                new FileSinkImages.CustomResolution(width, height)
        );
        fsi.writeAll(graph, filename);
    }
}
