
package GraphPack;

import MainPack.MainClass;
import Processes.LineParseCallable;
import Processes.MyProcess;
import View.MainFrame;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.graphstream.ui.swingViewer.ViewPanel;
import org.graphstream.ui.view.Viewer;
import org.graphstream.ui.view.ViewerPipe;

/**
 * Controller class for Graph visualization
 * @author bender
 */
public class MyGraphController
{
    private final LinkedList<MyProcess> ProcessList;
    private final MyGraph mGraph;
    private Viewer mViewer;
    private static final Logger LOGGER = Logger.getGlobal();
    private ViewerPipe mViewerPipe;


    /**
     * Create new controller instance
     * @param mainFrame valid window to display graph
     */
    public MyGraphController(MainFrame mainFrame)
    {
        ProcessList = new <MyProcess>LinkedList();
        mGraph = new MyGraph();
        mViewer = new Viewer(mGraph.getGraph(), Viewer.ThreadingModel.GRAPH_IN_ANOTHER_THREAD);
        mViewer.enableAutoLayout();

        // We connect back the viewer to the graph,
        // the graph becomes a sink for the viewer.
        // We also install us as a viewer listener to
        // intercept the graphic events.
        mViewerPipe = mViewer.newViewerPipe();
        mViewerPipe.addViewerListener(mainFrame);
        mViewerPipe.addSink(mGraph.getGraph());
    }


    
    @Override
    public void finalize() throws Throwable
    {
        mViewerPipe.clearSinks();
        mViewer.close();
        super.finalize();
    }


    /**
     * Throw away terminated processes
     * @param zombieList List of pids' for terminated processes
     */
    private void clearZombieNodes(LinkedList<MyProcess> zombieList)
    {
        if (zombieList == null)
            return;

        for(MyProcess p : zombieList) {
            mGraph.removeNode(String.valueOf(p.getPid()), p.getpName());
        }
    }
    
    /**
     * Get Graph Window
     * @return component which contains the graph
     */
    public ViewPanel getViewPanel()
    {
        if (mViewer != null)
            return mViewer.addDefaultView(false);
        return null;
    }


    /**
     * Retrieve processes from system using 'ps aux' command
     */
    private void getProcesses()
    {
        ExecutorService threadpool = Executors.newCachedThreadPool();
        LinkedList<MyProcess> tmpList = new LinkedList<>(ProcessList);
        ProcessList.clear();
        try {
            // Execute process list first
            Process p = Runtime.getRuntime().exec("ps aux");
            try (   // Get the input stream and read from it
                    BufferedReader input =
                    new BufferedReader
                    (new InputStreamReader(p.getInputStream()))
            ) {
                String line = input.readLine();             // spam line.
                while( (line = input.readLine()) != null )
                {
                    Future<MyProcess> future = threadpool.submit(new LineParseCallable(line)); // we get the process result for further processing
                    MyProcess mProcess = future.get();
                    if (mProcess != null) {
                        ProcessList.add(mProcess);
                    }
                }
            } catch (IOException ex) {
                LOGGER.log(Level.SEVERE, null, ex);
            } catch (InterruptedException | ExecutionException ex) {
                Logger.getLogger(MainClass.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                tmpList.removeAll(ProcessList);
                clearZombieNodes(tmpList);
                threadpool.shutdown();              // clear thread pool
            }
        }   catch (IOException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }
    }


    private MyProcess getProcessFromList(String cpid, LinkedList<MyProcess> processList)
    {
        ListIterator<MyProcess> li = processList.listIterator();
        int pid = Integer.parseInt(cpid);
        MyProcess p = null;
        while(li.hasNext())
        {
            p = li.next();
            if (p.getPid() == pid)
                return p;
        }
        return p;
    }


    /**
     * Retrieve system processes and create process graph
     */
    public void loop()
    {
        getProcesses();   // retrieve current running processes
        for(MyProcess mProcess : ProcessList)
        {
            LinkedList<String> cpids;
            try {
                cpids = mProcess.getChildren();
                cpids.stream().map((cpid) -> (MyProcess) getProcessFromList(cpid, ProcessList)).forEachOrdered((child) -> {
                    mGraph.addChild(mProcess, child);
                });                 
            } catch (IOException ex) {}
        }

        //copy back events  that have already occurred
        // in the viewer thread inside our thread.
        mViewerPipe.pump();
    }

    

    /**
     * Take a graph snapshot
     * @param filename name of the file
     * @param width image width
     * @param height image height
     */
    public void saveGraphImage(String filename, int width, int height)
    {
        if (mGraph == null)
            return;

        try {
            mGraph.saveGraphToPNG(filename, width, height);
        } catch (IOException ex) {
            Logger.getLogger(MyGraphController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    /**
     * Close all open resources
     */
    public void close(MainFrame frame)
    {
       mViewerPipe.clearSinks();
       mViewerPipe.removeViewerListener(frame);
    }

//    void setWindow(MainFrame mainFrame) {
//        Graph graph = mGraph.getGraph();
//        if (graph != null) {
//            Viewer viewer = new Viewer(graph, Viewer.ThreadingModel.GRAPH_IN_ANOTHER_THREAD);
//            ViewerPipe fromViewer = viewer.newViewerPipe();
//            fromViewer.addViewerListener(this);
//            fromViewer.addSink(graph);
//            ViewPanel view = viewer.addDefaultView(false);
//            if (view != null) {
//                mainFrame.add(view);
//                mainFrame.repaint();
//            }
//            
//        }
//    }
}
