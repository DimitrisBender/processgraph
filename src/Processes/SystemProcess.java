package Processes;

import static Processes.ProcessPermissions.SYSTEM;

/**
 *
 * @author bender
 */
public class SystemProcess extends MyProcess {
    
    public SystemProcess(String pName, String pUser, int pid, long totalMem, long memNow, long activeSince) {
        super(pName, pUser, pid, totalMem, memNow, activeSince);
        permissions = SYSTEM;
        cssValue = "size: 10px, 10px; fill-color: FireBrick;";
    }

    @Override
    public ProcessPermissions getPermissions() {
        return SYSTEM;
    }

    @Override
    public String getCSS() {
        return cssValue;
    }
    
}
