
package Processes;

import static Processes.ProcessPermissions.ROOT;

/**
 *
 * @author bender
 */
public class RootProcess extends MyProcess
{
    public RootProcess(String pName, String pUser, int pid, long totalMem, long memNow, long activeSince)
    {
        super(pName, pUser, pid, totalMem, memNow, activeSince);
        permissions = ROOT;
        cssValue = "size: 10px, 10px; fill-color: blue; " + 
                   "stroke-mode: plain; stroke-color: black;";
    }

    @Override
    public ProcessPermissions getPermissions() {
        return ROOT;
    }

    @Override
    public String getCSS() {
        return cssValue;
    }
}
