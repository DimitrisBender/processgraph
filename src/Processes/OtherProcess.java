package Processes;

import static Processes.ProcessPermissions.OTHER;

/**
 * Processes initialized by other users
 * @author bender
 */
public class OtherProcess extends MyProcess
{
    public OtherProcess(String pName, String pUser, int pid, long totalMem, long memNow, long activeSince)
    {
        super(pName, pUser, pid, totalMem, memNow, activeSince);
        permissions = OTHER;
        cssValue = "size: 8px, 8px; fill-color: LawnGreen;";
    }

    @Override
    public ProcessPermissions getPermissions() {
        return OTHER;
    }

    @Override
    public String getCSS() {
        return cssValue;
    }
}
