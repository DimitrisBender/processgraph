package Processes;

import static Processes.ProcessPermissions.ERROR;
import static Processes.ProcessPermissions.OTHER;
import static Processes.ProcessPermissions.ROOT;
import static Processes.ProcessPermissions.SYSTEM;
import static Processes.ProcessPermissions.USER;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Time;
import java.util.concurrent.Callable;
import java.util.logging.Logger;

public class LineParseCallable implements Callable<MyProcess>
{
    private final String mLine;
    public LineParseCallable(String line)
    {
        this.mLine = line;
    }


    private boolean isKernelThread(int id) throws FileNotFoundException, IOException
    {
        FileInputStream fis = new FileInputStream("/proc/" + String.valueOf(id) + "/cmdline");
        return fis.read() == -1;
    }


    private ProcessPermissions checkPermissions(String user, int pid)
    {
        String currentUsername = System.getProperty("user.name");
        try {
            if( isKernelThread(pid) )
                return SYSTEM;
        } catch (IOException ex) {
            return ERROR;
        }
        if (user.compareTo(currentUsername) == 0)
            return USER;
        else if( user.contentEquals("root") )     // it's a root process
            return ROOT;
        else                                // it's a simple process
            return OTHER;
    }


    @Override
    public MyProcess call()
    {
        MyProcess p = null;
        String[] processDetails = mLine.split("\\s+");
        String user = processDetails[0];                            // process ownership
        int pid = Integer.parseInt(processDetails[1]);              // pid
        float cpu = Float.parseFloat(processDetails[2]);            // current cpu usage
        float memory_prc = Float.parseFloat(processDetails[3]);     // percentage of system memory used by the process
        long vsz = Long.parseLong(processDetails[4]);               // Virtual memory Size, total amount of memory occupied by the process
        long rss = Long.parseLong(processDetails[5]);               // Resident set size. RAM Memory size currently occupied by the process
        String tty = processDetails[6];
        String stat = processDetails[7];        
        String start = processDetails[8] + ":59";                   // Started from
        String sysTime = processDetails[9];
        String CMD = processDetails[10];                            // name
        for(int i = 11; i<processDetails.length; ++i)
            CMD += " " + processDetails[i];

        switch(checkPermissions(user, pid))
        {
            case SYSTEM:
                SystemProcess tmpSysProcess = new SystemProcess(CMD, user, pid, vsz, rss, System.currentTimeMillis() - Time.valueOf(start).getTime());
                p = tmpSysProcess;
                break;
            case ROOT:
                RootProcess tmpRootProcess = new RootProcess(CMD, user, pid, vsz, rss, System.currentTimeMillis() - Time.valueOf(start).getTime());
                p = tmpRootProcess;
                break;
            case OTHER:
                OtherProcess tmpOtherProcess = new OtherProcess(CMD, user, pid, vsz, rss, System.currentTimeMillis() - Time.valueOf(start).getTime());
                p = tmpOtherProcess;
                break;
            case USER:
                UserProcess tmpUserProcess = new UserProcess(CMD, user, pid, vsz, rss, System.currentTimeMillis() - Time.valueOf(start).getTime());
                p = tmpUserProcess;
                break;
            default:
                Logger.getGlobal().info("unrecognised process status");
        }
        return p;
    }
}
