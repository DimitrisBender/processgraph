package Processes;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Abstract class representing a generic linux process
 * @author bender
 */
public abstract class MyProcess implements java.io.Serializable
{
    protected String cssValue;                      // css string        
    protected ProcessPermissions permissions;       // Process uid
    protected final String pName;                   // process cmd
    protected String pUser;                         // owner user
    protected int pid;
    protected final long activeSince;               // time since process init
    protected long memoryConsumptionTotal;          // total memory occupied (swap + RAM + libraries)
    protected long memoryConsumptionNow;            // total memory now (swap + RAM)
    private final Logger LOGGER = Logger.getLogger("PROCESS");

    /**
     * Generic constructor called only by subclasses
     * @param pName         // process name
     * @param pUser         // process owner
     * @param pid
     * @param totalMem      // total memory retrieved by ps aux
     * @param memNow        // memory used now
     * @param activeSince   // Creation date (as given by ps aux)
     */
    public MyProcess(String pName, String pUser, int pid, long totalMem, long memNow, long activeSince) {
        this.pName = pName;
        this.pUser = pUser;
        this.pid = pid;
        this.memoryConsumptionTotal = totalMem;
        this.memoryConsumptionNow = memNow;
        this.activeSince = activeSince;
        LOGGER.log(Level.FINEST, "Found process {0}:{1} of user {2}  active for {3}ms", new Object[]{pid, pName, pUser, activeSince});
    }

    /**
     * Empry constructor
     */
    public MyProcess()
    {
        this("COMMAND", "USER", 0, 0, 0, System.currentTimeMillis());
    }


    /**
     * Check if other Process is equal to this process
     * @param other - MyProcess to check
     * @return true if Processes are equal 
     * (this.pid == other.pid) && (this.name == other.name) && (this.user == other.user)
     */
    @Override
    public boolean equals(Object other)
    {
        if (other == null) return false;
        if (other == this) return true;
        if (!(other instanceof MyProcess))
            return false;
        MyProcess otherProcess = (MyProcess) other;
        return (this.getPid() == otherProcess.getPid()) &&
                ( this.getpName().equals(otherProcess.getpName()) ) &&
                ( this.getpUser().equals(otherProcess.getpUser()) );
                
    }


    /**
     * Get the value of activeSince
     *
     * @return the value of activeSince
     */
    public long getActiveSince() {
        return activeSince;
    }


    /**
     * Get the value of pUser
     *
     * @return the value of pUser
     */
    public String getpUser() {
        return pUser;
    }


    /**
     * Set the value of pUser
     *
     * @param pUser new value of pUser
     */
    public void setpUser(String pUser) {
        this.pUser = pUser;
    }


    /**
     * Get the value of pid
     *
     * @return the value of pid
     */
    public int getPid() {
        return pid;
    }


    /**
     * Set the value of pid
     *
     * @param pid new value of pid
     */
    public void setPid(int pid) {
        this.pid = pid;
    }


    /**
     * Get the value of memory_consumption
     *
     * @return the value of memory_consumption
     */
    public long getmemoryConsumptionTotal() {
        return memoryConsumptionTotal;
    }


    /**
     * Set the value of memory_consumption
     *
     * @param memory_consumption new value of memory_consumption
     */
    public void setmemoryConsumptionTotal(long memory_consumption) {
        this.memoryConsumptionTotal = memory_consumption;
    }


    /**
     * Get the value of memoryConsumptionNow
     *
     * @return the value of memoryConsumptionNow
     */
    public long getMemoryConsumptionNow() {
        return memoryConsumptionNow;
    }


    /**
     * Set the value of memoryConsumptionNow
     *
     * @param memoryConsumptionNow new value of memoryConsumptionNow
     */
    public void setMemoryConsumptionNow(long memoryConsumptionNow) {
        this.memoryConsumptionNow = memoryConsumptionNow;
    }
    
    
    public abstract ProcessPermissions getPermissions();
    public abstract String getCSS();

    /**
     * Get the value of pName
     *
     * @return the value of pName
     */
    public String getpName() {
        return pName;
    }
    

    // retrieve /proc entry
    private String getProcessFileDirectory()
    {
        String sPid = String.valueOf(this.getPid());
        return "/proc/" + sPid + "/task/" + sPid + "/children";
    }


    // add children pid to children list
    private void addProcessList(LinkedList<String> list, String cpid)
    {
        if( !cpid.isEmpty() )
        {
            list.add(cpid);
            LOGGER.log(Level.FINEST, "Children with pid: {0}", cpid);   
        }
    }
    
    /**
     * Get process children
     * @return a LinkedList of the pids of process Children.
     * @throws java.io.FileNotFoundException unexpected process miss
     */
    public LinkedList<String> getChildren() throws FileNotFoundException, IOException
    {
        // Get children pids from /proc/$'pid'/task/$'pid'/children
        String childrenFileDir = getProcessFileDirectory();

        // open file
        FileInputStream fis = new FileInputStream(childrenFileDir);

        // create a list of String pids
        LinkedList<String> childrenpids = new LinkedList<>();

        try
        {
            int r;
            String tmp = "";

            while(true) {                               // RUN FOR-EVER BUAHAHA
                r = fis.read();
                char c = (char) r;                      // convert result to a character
                if (c == ' ') {                         // split whitespace
                    addProcessList(childrenpids, tmp);  // add new pid to list
                    tmp = "";
                } else if (r == -1) {                   // EOF
                    addProcessList(childrenpids, tmp);
                    break;
                } else
                    tmp += c;
            }
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        } finally {
            fis.close();
            return childrenpids;
        }
    }
}
