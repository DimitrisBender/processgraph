
package Processes;

import static Processes.ProcessPermissions.USER;

/**
 * Processes initialized by current user
 * @author bender
 */
public class UserProcess extends MyProcess {
    
    public UserProcess(String pName, String pUser, int pid, long totalMem, long memNow, long activeSince) {
        super(pName, pUser, pid, totalMem, memNow, activeSince);
        permissions = USER;
        cssValue = "size: 10px, 10px; fill-color: ForestGreen; " + 
                   "stroke-mode: plain; stroke-color: black;";
    }

    @Override
    public ProcessPermissions getPermissions() {
        return USER;
    }

    @Override
    public String getCSS() {
        return cssValue;
    }
    
}
